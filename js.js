let table = document.querySelector("#table");
let buttonNewGame = document.getElementById("button-new-game");
let height = 10;
let width = 10;
let winnerMarksCount = 5;
let clicks = 0;
let stop = false;

// ZMENA HRACOV
const getPlayer = () => {
    return clicks % 2 === 0 ? "V" : "M";
};

// KONTROLA VITAZSTVA
const testWin = (x, y) => {
    let clickCell = table.rows[y].cells[x].innerHTML;

    const subTest = (moveX, moveY) => {
        let count = 1;

        const test = (sygn) => {
            let actualX = x + moveX * sygn;
            let actualY = y + moveY * sygn;

            while (table.rows[actualY] && table.rows[actualY].cells[actualX] && table.rows[actualY].cells[actualX].innerHTML === clickCell) {
                count++;
                actualX += moveX * sygn;
                actualY += moveY * sygn;
            }
        };

        test(1);
        test(-1);

        if (count >= winnerMarksCount) {
            console.log(clickCell + " winner");
            stop = true;
            return true;
        }
        return false;
    };

    subTest(1, 0) || subTest(0, 1) || subTest(1, 1) || subTest(1, -1);

};

const cleanTable = () => {
    table.innerHTML = "";
    clicks = 0;
    stop = false;
};

// VYTVORENIE TABULKY
const createTable = () => {
    cleanTable();
    for (let i=0; i<height; i++) {
        let row = document.createElement("tr");
        for (let j=0; j<width; j++) {
            let cell = document.createElement("td");
            cell.classList.add("cell");
            let callback = () => {
                cell.removeEventListener("click", callback);
                if (stop) {
                    return;
                }
                clicks++;
                cell.innerHTML = getPlayer();
                if (clicks >= (winnerMarksCount * 2) - 1) {
                    testWin(j, i);
                }
            };

            cell.addEventListener("click", callback);

            row.appendChild(cell);
        }
        table.appendChild(row);
    }
};

createTable();

buttonNewGame.addEventListener("click", () => {
    createTable();
});
